import React from "react";
import {CategoriesService,url} from "../../Services/Services"
import { Link } from "react-router-dom";
import {FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft} from '@fortawesome/free-solid-svg-icons';
import "./Home.scss";

class Home extends React.Component{

    state={
        category_lists:[]
    }

    componentDidMount(){
        CategoriesService()
        .then(response => response.json())
        .then(data => {
            this.setState({
              category_lists:[...data]
            })
        })
    }

    render(){
        const{category_lists}=this.state;
        return(
            <div className="container_category">
                <h2>دسته بندی</h2>
                <div className="category d_flex_between">
                    {category_lists.map(item => (
                        <div key={item.id} className="products_category">
                            <Link className="link" to={`/categories/${item.id}`}>
                                <div className="product_box d_flex_around">
                                    <img src={`${url}${item.image}`} alt="" width="200" height="200"/>
                                    <div className="title">
                                        {item.name}
                                        <FontAwesomeIcon className="arrow_icon" icon={faChevronCircleLeft}/>
                                    </div>
                                </div>
                            </Link>
                        </div>)
                    )}
                </div>
            </div>
        )
    }
}

export default Home;