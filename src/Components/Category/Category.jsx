import React from "react";
import {CategoriesService} from "../../Services/Services"
import { Link } from "react-router-dom";
import "./Category.scss";

class Category extends React.Component{

    state={
        category_lists:[]
    }

    componentDidMount(){
        const{match}=this.props;
        CategoriesService()
        .then(response => response.json())
        .then(data => {
            const categoryLists = data.find(({id}) => id === match.params.id);
            this.setState({
              category_lists:[...categoryLists.children]
            })
        })
    }

    componentDidUpdate(prevProps){
        const{match}=this.props;
        if(prevProps.match.params.id !== match.params.id){
            CategoriesService()
            .then(response => response.json())
            .then(data => {
                const categoryLists = data.find(({id}) => id === match.params.id);
                this.setState({
                category_lists:[...categoryLists.children]
                })
            })
        }
    }

    render(){
        const{category_lists}=this.state;
        return(
            <div className="category">
                <h2>دسته بندی</h2>
                <div className="category_types d_flex_start">
                    {category_lists.map(item => (
                        <div key={item.id} className="category__types_box">
                            <Link className="links" to={`/products/${item.id}`}>
                                <div className="box">
                                    <div className="title">{item.name}</div>
                                </div>
                            </Link>
                        </div>)
                    )}
                </div>
            </div>
        )
    }
}

export default Category;