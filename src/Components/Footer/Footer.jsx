import React from "react";
import logo from "../../images/logo.png";
import {FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhoneVolume ,faEnvelope} from '@fortawesome/free-solid-svg-icons';
import { faInstagram ,faYoutube,faTwitter,faFacebookF} from '@fortawesome/free-brands-svg-icons';
import "./Footer.scss";

class Footer extends React.Component{
    render(){
        return(
            <footer>
                <div className="footer__top d_flex_between">
                    <div className="footer__top_right d_flex_between">
                        <p>مرکز پشتیبانی بازاریابان </p>
                        <p>
                            <span>021 88 88 88 88</span>
                            <span>
                                <FontAwesomeIcon className="icon" icon={faPhoneVolume}/>
                            </span>
                        </p>
                        <p>
                            <span>sellersupport@liateam.com</span>
                            <span>
                                <FontAwesomeIcon className="icon" icon={faEnvelope}/>
                            </span>
                        </p>
                    </div>
                    <div className="footer__top_left d_flex_between">
                        <p>Good Time Good News</p>
                        <p>Lia Virtual Office</p>
                        <div className="logo">
                            <img src={logo} alt="لوگو" width="100%" height="100%"/>
                        </div>
                    </div>
                </div>
                <div className="footer__bottom d_flex_between">
                    <p>هفت روز هفته ، ۲۴ ساعت شبانه‌روز پاسخگوی شما هستیم</p>
                    <div className="social_icon">
                        <span>
                            <FontAwesomeIcon className="icon" icon={faYoutube}/>
                        </span>
                        <span>
                            <FontAwesomeIcon className="icon" icon={faTwitter}/>
                        </span>
                        <span>
                            <FontAwesomeIcon className="icon" icon={faFacebookF}/>
                        </span>
                        <span>
                            <FontAwesomeIcon className="icon" icon={faInstagram}/>
                        </span>
                    </div>
                    <p>© تمام حقوق این وب سایت متعلق به شرکت آرمان تدبیر اطلس 1398-۱۳۹7 می باشد</p>
                </div>
            </footer>
        )
    }
}

export default Footer;