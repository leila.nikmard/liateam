import React from "react";
import {ProductsService ,url} from "../../Services/Services";
import {connect} from "react-redux";
import actions from "../../actions/actions";
import {FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartPlus} from '@fortawesome/free-solid-svg-icons';
import "./Product.scss";

class Products extends React.Component{
    state = {
        products_list:[]
    }
    componentDidMount(){
        const{match}=this.props;
        ProductsService(match.params.id)
        .then(response => response.json())
        .then(data => this.setState({
            products_list:[...data.list]
        }))
    }

    render(){
        const{products_list}=this.state;
        const{addProduct}=this.props;
        return(
            <div className="container_products">
                <h2>محصولات</h2>
                <div className="products d_flex_start">
                    {products_list.map(item => (
                        <div key={item.id} className="product_box">
                            <div className="product_image">
                                <img src={`${url}${item.medium_pic}`} alt="" width="100%"/>
                            </div>
                            <p className="title">{item.title}</p>
                            <p className="volume">{item.volume}</p>
                            <div className="price"><span>{item.price.final_price}</span><span>تومان</span></div>
                            <span onClick={(event) => addProduct(item)}>
                                <FontAwesomeIcon className="cart_plus" icon={faCartPlus}/>
                            </span>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        addProduct : function(payload){
        dispatch({
          type:actions.ADD_PRODUCT,
          payload
        })
      }
    }
  }

export default connect(null,mapDispatchToProps)(Products);