import { library } from '@fortawesome/fontawesome-svg-core'
import {faChevronCircleLeft,faUser,faEnvelope,faPhoneVolume,faShoppingCart,faCartPlus,faTimes} from '@fortawesome/free-solid-svg-icons';
import {faInstagram ,faYoutube,faTwitter,faFacebookF} from '@fortawesome/free-brands-svg-icons'

 
library.add(faChevronCircleLeft,faUser,faInstagram,faTwitter,faYoutube,faEnvelope,faPhoneVolume,faFacebookF,faShoppingCart,faCartPlus,faTimes)