import React from "react";
import {Link} from "react-router-dom";
import {url} from "../../Services/Services"
import logo from "../../images/logo.png";
import {connect} from "react-redux";
import {FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser,faShoppingCart,faTimes} from '@fortawesome/free-solid-svg-icons';
import "./Header.scss";

class Header extends React.Component{

    state={
        show_shoppin: true,
    }

    handleClick = () => {
        this.setState(({
            show_shoppin : !this.state.show_shoppin
        }));
    }
    render(){
        const{show_shoppin}=this.state;
        const{cart}=this.props;
        return(
            <header>
                <div className="header_top d_flex_between">
                    <div className="right d_flex_between">
                        <div className="logo d_flex_start">
                            <img src={logo} width="100" height="100" alt="لوگو"/>
                        </div>
                        <nav>
                            <ul>
                                <li><Link className="link" to="/">خانه</Link></li>
                                <li><Link to="#" className="link">فروشگاه</Link></li>
                                <li><Link to="#" className="link">وبلاگ</Link></li>
                                <li><Link to="#" className="link">درباره ما</Link></li>
                                <li><Link to="#" className="link">تماس با ما</Link></li>
                            </ul>
                        </nav>
                    </div>
                    <div className="left">
                        <div className="profile d_flex_between">
                            <span>رضا پور جباری عزیز</span>
                            <div className="user">
                                <FontAwesomeIcon className="user__icon" icon={faUser}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header_bottom d_flex_between">
                    <nav>
                        <ul className="d_flex_end">
                            <li><Link className="link" to="/categories/10">مراقبت پوست</Link></li>
                            <li><Link className="link" to="/categories/11">مراقبت مو</Link></li>
                            <li><Link className="link" to="/categories/90">مراقبت بدن</Link></li>
                            <li><Link className="link" to="/categories/91">آرایشی</Link></li>
                            <li><Link className="link" to="#">پرفروشترین</Link></li>
                            <li><Link className="link" to="#">جدیدترین</Link></li>
                        </ul>
                    </nav>
                    <div className="buy" onClick={this.handleClick}>
                        <div className="quantity_shopping">{cart.length}</div>
                        <FontAwesomeIcon className="shopping_icon" icon={faShoppingCart}/>
                        <div className="shopping" hidden={show_shoppin}>
                            <ul>
                                {cart.length > 0 ? cart.map(item => (
                                    <li key={item.id}>
                                        <div className="left">
                                            <img src={`${url}${item.medium_pic}`} alt="" width="60"/>
                                        </div>
                                        <div className="right">
                                            <p>{item.title}</p>
                                            <p>{item.price.price} <span>تومان</span></p>
                                            <span className="float">
                                                <FontAwesomeIcon icon={faTimes}/>
                                            </span>
                                            <div className="quantity_selector">
                                                <span>-</span>
                                                <span>1</span>
                                                <span>+</span>
                                            </div>
                                        </div>
                                    </li>
                                )) : "سبد خرید شما خالی است!"}
                            </ul>
                            {cart.length > 0 ? <>
                                <div className="checkout_bill">
                                <p>
                                    <span>جمع کل:</span>
                                    <span>545 تومان</span>
                                </p>
                                <p>
                                    <span>مبلغ قابل پرداخت:</span>
                                    <span>546 تومان</span>
                                </p>
                            </div>
                            <button className="checkout__bill_button" type="submit">ثبت سفارش</button>
                            </>
                            : null}
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      cart : state.cart
    }
  }

export default connect(mapStateToProps,null)(Header);