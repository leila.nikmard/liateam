import React from 'react';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import {Route ,Switch} from "react-router-dom";
import Home from "./Components/Home/Home";
import Category from "./Components/Category/Category";
import Products from "./Components/Products/Products";
import "./Components/FontAwsomeIcons/index";
import "./App.scss";


class App extends React.Component {

  render(){
    return(
      <main>
        <Header/>
        <div className="container">
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/categories/:id" component={Category}/>
            <Route path="/products/:id" component={Products}/>
          </Switch>
        </div>
        <Footer />
      </main>
    )
  }
 
}

export default App;
