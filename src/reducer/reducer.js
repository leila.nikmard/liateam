import actions from "../actions/actions";
const initState = {
    cart:[]
}

const reducer = (state = initState , action) => {
    let newState = state;
    switch(action.type){
        case actions.ADD_PRODUCT:
            newState = {...state , cart:[...state.cart , action.payload]}
        break;
        default:
            newState = state;
        break;
    }
    return newState;

}

export default reducer;